// main.js

import Vue from "vue";
import App from "./App.vue";
import VueRouter from "vue-router";

Vue.use(VueRouter);

const routes = [
  { path: "/index.html", name: "home", component: Home },
  { path: "/about.html", name: "about", component: About },
  { path: "/services.html", name: "services", component: Services },
  // Add more routes as needed
];

const router = new VueRouter({
  routes,
  mode: "history", // Use history mode for cleaner URLs
});

new Vue({
  render: (h) => h(App),
  router,
}).$mount("#app");
