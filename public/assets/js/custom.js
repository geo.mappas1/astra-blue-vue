(function ($) {
  "use strict";

  // Mean Menu
  jQuery(".mean-menu").meanmenu({
    meanScreenWidth: "991",
  });

  //Navbar JS
  $(window).on("scroll", function () {
    if ($(this).scrollTop() > 150) {
      $(".navbar-area").addClass("sticky-top");
    } else {
      $(".navbar-area").removeClass("sticky-top");
    }
  });

  //Search Bar
  $(".search-icon").on("click", function () {
    $(".search-form").toggle();
  });

  //testimonial Slider
  $(".testimonial-slider").owlCarousel({
    loop: true,
    margin: 10,
    nav: true,
    items: 1,
    smartSpeed: 2000,
    dots: false,
  });

  // Showcase Portfolio
  try {
    var mixer = mixitup("#Container", {
      controls: {
        toggleDefault: "none",
      },
    });
  } catch (err) {}

  //Accordian JS
  $(".faq-area .open").click(function () {
    var container = $(this).parents(".topic");
    var answer = container.find(".answer");
    var trigger = container.find(".faq-t");

    answer.slideToggle(200);

    if (trigger.hasClass("faq-o")) {
      trigger.removeClass("faq-o");
    } else {
      trigger.addClass("faq-o");
    }

    if (container.hasClass("expanded")) {
      container.removeClass("expanded");
    } else {
      container.addClass("expanded");
    }
  });

  // Video JS
  $(".popup-vimeo").magnificPopup({
    disableOn: 320,
    type: "iframe",
    mainClass: "mfp-fade",
    removalDelay: 160,
    preloader: false,

    fixedContentPos: false,
  });

  //Popup Gallery
  $(".popup-gallery").magnificPopup({
    type: "image",
  });

  // Subscribe form
  $(".newsletter-form")
    .validator()
    .on("submit", function (event) {
      if (event.isDefaultPrevented()) {
        // handle the invalid form...
        formErrorSub();
        submitMSGSub(false, "Please enter your email correctly.");
      } else {
        // everything looks good!
        event.preventDefault();
      }
    });
  function callbackFunction(resp) {
    if (resp.result === "success") {
      formSuccessSub();
    } else {
      formErrorSub();
    }
  }
  function formSuccessSub() {
    $(".newsletter-form")[0].reset();
    submitMSGSub(true, "Thank you for subscribing!");
    setTimeout(function () {
      $("#validator-newsletter").addClass("hide");
    }, 4000);
  }
  function formErrorSub() {
    $(".newsletter-form").addClass("animate__animated animate__shake");
    setTimeout(function () {
      $(".newsletter-form").removeClass("animate__animated animate__shake");
    }, 1000);
  }
  function submitMSGSub(valid, msg) {
    if (valid) {
      var msgClasses = "validation-success";
    } else {
      var msgClasses = "validation-danger";
    }
    $("#validator-newsletter").removeClass().addClass(msgClasses).text(msg);
  }

  // AJAX MailChimp
  $(".newsletter-form").ajaxChimp({
    url: "https://envytheme.us20.list-manage.com/subscribe/post?u=60e1ffe2e8a68ce1204cd39a5&amp;id=42d6d188d9", // Your url MailChimp
    callback: callbackFunction,
  });

  //Pre Loader
  $(window).on("load", function () {
    $(".loader-content").fadeOut(1000);
  });

  // Switch Btn
  // $('body').append("<div class='switch-box'><label id='switch' class='switch'><input type='checkbox' onchange='toggleTheme()' id='slider'><span class='slider round'></span></label></div>");
})(jQuery);

// function to set a given theme/color-scheme
function setTheme(themeName) {
  localStorage.setItem("everb_theme", themeName);
  document.documentElement.className = themeName;
}
// function to toggle between light and dark theme
function toggleTheme() {
  if (localStorage.getItem("everb_theme") === "theme-dark") {
    setTheme("theme-light");
  } else {
    setTheme("theme-dark");
  }
}

async function changeLang(language, el) {
  const container = document.querySelector(".chooseLang").classList;

  if (container.contains("open")) {
    container.remove("open");
    if (!el.classList.contains("chosen")) {
      document.querySelector(".chooseLang .chosen").classList.remove("chosen");
      el.classList.add("chosen");
      changeLanguage(language);
    }
    return;
  }

  container.add("open");
}

async function changeLanguage(lang) {
  try {
    await setLanguagePreference(lang);
    const langData = await fetchLanguageData(lang);
    updateContent(langData);
  } catch (error) {
    console.error("Error changing language:", error);
  }
}

function updateContent(langData) {
  document.querySelectorAll("[data-i18n]").forEach((element) => {
    const key = element.getAttribute("data-i18n");
    element.textContent = langData[key] || "";
  });
}

function setLanguagePreference(lang) {
  localStorage.setItem("language", lang);
}

async function fetchLanguageData(lang) {
  const response = await fetch(`/assets/js/languages/${lang}.json`);
  if (!response.ok) {
    throw new Error("Failed to fetch language data");
  }
  return response.json();
}
// Immediately invoked function to set the theme on initial load
// (function () {
//   // Check if the "everb_theme" key exists in localStorage
//   if (localStorage.getItem("everb_theme") === "theme-dark") {
//     // If the theme is set to dark in localStorage, set the theme to dark
//     setTheme("theme-dark");
//     // Set the slider to unchecked (assuming it's a toggle switch for light/dark theme)
//     document.getElementById("slider").checked = false;
//   } else {
//     // If the theme is set to light in localStorage, set the theme to light
//     setTheme("theme-light");
//     // Set the slider to checked
//     document.getElementById("slider").checked = true;
//   }
// })();
